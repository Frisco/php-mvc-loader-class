<?php

 class Loader
 {
	/**
	 * Singleton instance
	 */
	protected static $_instance = null;
	
	/**
	 * Controller Directory Path
	 *
	 * @var Array
	 * @access protected
	 */
	protected $_controllerDirectoryPath = array();
	
	/**
	 * Model Directory Path
	 *
	 * @var Array
	 * @access protected
	 */
	protected $_modelDirectoryPath = array();
	
	/**
	 * View Directory Path
	 *
	 * @var Array
	 * @access protected
	 */
	protected $_viewDirectoryPath = array();
	
	/**
	 * Library Directory Path
	 *
	 * @var Array
	 * @access protected
	 */
	protected $_libraryDirectoryPath = array();
	
	/**
	 * Config Directory Path
	 *
	 * @var Array
	 * @access protected
	 */
	protected $_configDirectoryPath = array();	
	
	/**
	 * Core Library Directory Path
	 *
	 * @var Array
	 * @access protected
	 */
	protected $_coreLibraryDirectoryPath = null;	
	
    /**
     * Singleton instance
     *
     * @return Loader instance
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
	
	/** 
	 * Constructor: Set what is need be loaded
	 ** @access public
	 */
	 
	public function __construct()
	{
		$this->_modelDirectoryPath        = MPATH;
		$this->_viewDirectoryPath         = VPATH;
		$this->_controllerDirectoryPath   = CPATH;
		$this->_libraryDirectoryPath      = LPATH;
		$this->_coreLibraryDirectoryPath  = LIBPATH;
		$this->_configDirectoryPath       = CONFIGPATH;
		
		spl_autoload_register(array($this,'user_controller'));
		spl_autoload_register(array($this,'user_model'));
		spl_autoload_register(array($this,'user_library'));
		spl_autoload_register(array($this,'system_library'));
		
		log_message('debug',"Loader Class Initialized");
	}

	/** 
	 *-----------------------------------------------------
	 * Load Specific Library
	 *-----------------------------------------------------
	 * Method for load specific library.
	 * This method return class object.
	 *
	 * @library String
	 * @param String
	 * @access public
	 */	
	public function load_specific_library($library, $param = null)
	{
		if (is_string($library)) {
			return $this->initialize_class($library);
		}
		if (is_array($library)) {
			foreach ($library as $key) {
				return $this->initialize_class($library);
			}
		}				
	}

	/** 
	 *-----------------------------------------------------
	 * Initialize Class
	 *-----------------------------------------------------
	 * Method for initialise class
	 * This method return new object. 
	 * This method can initialize more class using (array)
	 *
	 * @library String|Array
	 * @param String
	 * @access public
	 */	
	public function initialize_class($library)
	{
		try {
			if (is_array($library)) {
				foreach($library as $class) {
					$arrayObject =  new $class;
				}			
				return $this;
			}
			if (is_string($library)) {
				$stringObject = new $library;
			}else {
				throw new ISException('Class name must be string.');
			}
			if (null == $library) {
				throw new ISException('You must enter the name of the class.');
			}
		} catch(Exception $exception) {
			echo $exception;
		}
	}	

	/**
     * Autoload all user library class
     *
     * @param  string $class
     * @return object
	 * @access public
     */
	 
    public function user_library($lib)
    {
        if ($lib) {
			set_include_path($this->_libraryDirectoryPath);
			spl_autoload_extensions('.php');
			spl_autoload($lib);
		}
    }
	/**
     * Autoload all system library class
     *
     * @param  string $class
     * @return object
	 * @access public
     */
	 
    public function system_library($lib)
    {
        if ($lib) {
			set_include_path($this->_coreLibraryDirectoryPath);
			spl_autoload_extensions('.php');
			spl_autoload($lib);
		}
    }	
	/**
     * Autoload all user model class
     *
     * @param  string $class
     * @return object
	 * @access public
     */
	 
    public function user_model($model)
    {
        if ($model) {
			set_include_path($this->_modelDirectoryPath);
			spl_autoload_extensions('.php');
			spl_autoload($model);
		}
    }	
	
	/**
     * Autoload all user controller class
     *
     * @param  string $class
     * @return object
	 * @access public
     */
	 
    public function user_controller($controller)
    {
        if ($controller) {
			set_include_path($this->_controllerDirectoryPath);
			spl_autoload_extensions('.php');
			spl_autoload($controller);
		}
    }	
	
	/**
     * Load Controller
	 *
     * This method load specific controller 
	 *
     * @param  string $class
     * @return object
	 * @access public
     */	
	public function controller($name)
	{
		return $this->load_specific_library($name);
	}
  
	/**
     * Load Model
	 *
     * This method load specific user model
	 *
     * @param  string $class
     * @return object
	 * @access public
     */	
	public function model($name)
	{
		return $this->load_specific_library($name);
	}
	
	/**
     * Load Library
	 *
     * This method load specific user library
	 *
     * @param  string $class
     * @return object
	 * @access public
     */	
	public function library($name)
	{
		return $this->load_specific_library($name);
	} 
	
	/**
     * Load Core Library
	 *
     * This method load specific core library
	 *
     * @param  string $class
     * @return object
	 * @access public
     */	
	public function core_library($name)
	{
		return $this->load_specific_library($name);
	} 
	
	/**
     * Load Config
	 *
     * This method for loading single or array config file 
	 *
     * @param  string $class
	 * @access public
     */	

	public function config($name)
	{
		$path = $this->_configDirectoryPath.$name.EXT;
		if (is_array($name)) {
			foreach ($name as $key) {
				require_once $this->_configDirectoryPath.$key.EXT;
			}
		}else {
			 include $path;
		}

	}
	
	/**
     * Load Config Item
	 *
	 *
     * @param  string $class
	 * @access public
     */	
	public function item($value,$key,$conf) 
	{
		return $config[$key][$value] = $conf;
	}

 }
 
 
 
 
 
 
 
 
 
 
 
 
 
